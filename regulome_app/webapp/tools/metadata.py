# Global variables
VERSION = (2, 0, 0)
__package_name__ = 'regulome_web'
__author__ = 'Loris Mularoni'
__version__ = ".".join([str(i) for i in VERSION])
__description__ = 'Human Islet Regulome Browser'

