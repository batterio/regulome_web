#!/soft/virtenvs/regulome/bin/python
activate_this = '/soft/virtenvs/regulome/bin/activate_this.py'
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))


# Import modules
import sys
import os
import logging

sys.path.insert(0, '/data/deploy/')
os.chdir('/data/deploy/')


# Set the logging
logging.basicConfig(stream=sys.stderr)


CONFIGURATION = 'development'
try:
    from regulome_app.webapp.app import app as application
    from regulome_app.config import web_configuration
    application.config.from_object(web_configuration[CONFIGURATION])
except Exception as e:
    logging.exception(e)
